# Focal Length Photography
This repo contains the source files for the FLP website.

https://focallengthphotography.com/

Testing URL: http://flp.clients.keengoat.com/

This site is powered by [Hugo](https://gohugo.io/) and hosted by [Netlify](http://netlify.com/). Content editing is handled by [Forestry](http://forestry.io/).

Content edits are completed in Forestry which saves by commiting changes to this gitlab repo. Netlify then runs the build process as soon as a change is pushed to ``master``, and sets the changes live.

## Content Maintenence
All content changes should be handled through the [Forestry](http://forestry.io/) account. This acts in much the same way that the development process does. All changes are logged as commits in this gitlab repo. 

1. Visit https://forestry.io/ and login
2. Select the FLP site
3. Edit the pages or content as needed
4. Toggle draft mode on or off according to suit needs
5. When finished, click Save to publish changes live

For more detailed information visit the official [Forestry documentation](https://forestry.io/docs/welcome/)

## Local setup and install
- Clone the repo and navigate to the newly created directory

```
$ git clone git@gitlab.com:focallengthphotography/website.git
$ cd website
```

- Install hugo by following the [installation guide here](https://gohugo.io/getting-started/installing/)

- Run hugo to setup a live server to view and develop

```
$ hugo serve
```

Note:
- ``master`` is the production branch
- Ensure that all development is conducted on the ``dev`` branch.

## Todos
The project is currently not yet complete. Here are some outstanding items I have planned in no particular order.

- [ ] Contact form powered by netflify
- [ ] Gallery page
- [ ] Packages / pricing / services page
- [ ] Dark black colour theme (subject to feedback of course)
- [ ] Improve design and colours based on feedback
- [ ] Source logo
- [ ] Setup google analytics
- [ ] Add footer with social icons and copyright information
- [ ] Gain feedback on wesite copy and revise accordingly
- [ ] Phone call or meeting with Chelsea to gather feedback