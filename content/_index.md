---
title: Home
date: 2018-07-26 14:00:00 +0000
description: Photography services for every occasion!
header:
  description: Welcome to <span style="color:#F9690E">Focal Length Photography</span>.
    Let's capture a beautiful moment.
  image:
    url: img/home_img.jpg
    alt_text: 
    responsive_sources:
      '360': img/home_360x318.png
      '565': img/home_565x420.png
      '848': img/home_848x443.png
text_groups:
- name: What we do
  description: Focal Length Photography are a team of Melbourne based photographers
    with a passion for producing excellent images to suit needs ranging from families
    and portraits to business and events. Please reach out to our friendly team to
    take the next step in capturing your moment.
projects:
- title: Portrait
  type: Capturing a feeling
  link: 
  image:
    url: img/portrait_1130x590.jpg
    alt_text: Potrait photograph of woman in black
    responsive_sources:
      '360': img/portrait_360x318.jpg
      '565': img/portait_565x420.jpg
      '848': img/portrait_848x443.jpg
- title: Landscape
  type: Sharing a place
  link: 
  class: short-col
  image:
    url: img/landscape_364x590.jpg
    alt_text: The Analytic web design theme
    responsive_sources:
      '360': img/landscape_360x318.jpg
      '565': img/landscape_565x420.jpg
      '848': img/landscape_848x443.jpg
- title: Family
  type: Cherishing a memory
  link: 
  class: wide-col
  image:
    url: img/family_746x590.jpg
    alt_text: The Friends theme
    responsive_sources:
      '360': img/family_360x318.jpg
      '565': img/family_565x420.jpg
      '848': img/family_848x443.jpg

---
